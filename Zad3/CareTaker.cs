﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class CareTaker
    {
        private List<Memento> history;
        public CareTaker()
        {
            history = new List<Memento>();
        }

        public Memento PreviousState { get; set; }
        public void AddMemento(Memento memento)
        {
            history.Add(memento);
        }
        public void RemoveMemento(Memento memento)
        {
            history.Remove(memento);
        }
        public Memento GetMemento()
        {
            PreviousState = history.Last();
            history.Remove(PreviousState);
            return PreviousState;
        }
    }
}
