﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            string dateInput = "May 14, 2020, 23:00";
            ToDoItem item = new ToDoItem("LV6", "6 vježba iz labosa iz RPPOONA", DateTime.Parse(dateInput));
            CareTaker caretaker = new CareTaker();
            caretaker.AddMemento(item.StoreState());
            item.Rename("Laboratorijska Vjezba 6");
            caretaker.AddMemento(item.StoreState());
            item.ChangeTask("6. vježba iz laboratorijskih iz RPPOONA"); 
            Console.WriteLine(item.ToString());
            item.RestoreState(caretaker.GetMemento());
            Console.WriteLine(item.ToString());
            item.RestoreState(caretaker.GetMemento());
            Console.WriteLine(item.ToString());
        }
    }
}
