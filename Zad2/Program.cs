﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product("Cheese", 2.99);
            Product product2 = new Product("Bacon Strips", 6.99);
            Product product3 = new Product("Coca Cola", 4);
            Box firstBox = new Box();
            firstBox.AddProduct(product1);
            firstBox.AddProduct(product2);
            firstBox.AddProduct(product3);
            IAbstractIterator iterator = firstBox.GetIterator();
            for(int i = 0; i < firstBox.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }
        }
    }
}
