﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker DigitChecker = new StringDigitChecker();
            StringChecker LengthChecker = new StringLengthChecker();
            StringChecker UpperCaseChecker = new StringUpperCaseChecker();
            StringChecker LowerCaseChecker = new StringLowerCaseChecker();
            DigitChecker.SetNext(LengthChecker);
            LengthChecker.SetNext(UpperCaseChecker);
            UpperCaseChecker.SetNext(LowerCaseChecker);
            Console.WriteLine("Provjera prvog stringa:");
            DigitChecker.Check("RPPOONLv6");
            Console.WriteLine("Provjera drugog stringa:");
            DigitChecker.Check("Razvoj Programske Podrške");

        }
    }
}
