﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6
{
    class StringLowerCaseChecker: StringChecker
    {
        protected override bool PerformCheck(string stringtoCheck)
        {
            if (stringtoCheck.Any(char.IsLower))
            {
                Console.WriteLine("String ima barem jedno malo slovo");
                return true;
            }
            else
            {
                Console.WriteLine("String nema barem jedno malo slovo");
                return false;
            }
        }
    }
}
