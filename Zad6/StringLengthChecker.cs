﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6
{
    class StringLengthChecker: StringChecker
    {
        protected override bool PerformCheck(string stringtoCheck)
        {
            if (stringtoCheck.Length>= 6)
            {
                Console.WriteLine("String ima potreban broj znakova.");
                return true;
            }
            else
            {
                Console.WriteLine("String nema potreban broj znakova.");
                return false;
            }
        }
    }
}
