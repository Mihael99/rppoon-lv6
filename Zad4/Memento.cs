﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Memento
    {
        public string ownername { get; private set; }
        public string owneraddress { get; private set; }
        public decimal Balance { get; private set; }
        public Memento(string OwnerName, string OwnerAddress, decimal balance)
        {
            this.ownername = OwnerName;
            this.owneraddress = OwnerAddress;
            this.Balance = balance; 
        }
    }
}
