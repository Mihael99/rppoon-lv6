﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount("John", "221 Baker Street", 850.77m);
            Memento firstMemento = account.StoreState();
            account.UpdateBalance(1440.24m);
            Console.WriteLine(account.ToString());
            account.RestoreState(firstMemento);
            Console.WriteLine(account.ToString());
        }
    }
}
