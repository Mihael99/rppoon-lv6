﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL); 
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "E:\\logFile.txt");
            fileLogger.SetNextLogger(logger);
            fileLogger.Log("Prvi log.", MessageType.INFO);
            fileLogger.Log("Prvi log.", MessageType.WARNING);
            fileLogger.Log("Prvi log.", MessageType.ERROR);
            fileLogger.Log("Prvi log.", MessageType.ALL);
        }
    }
}
