﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote = new Note("First note", "This is my first note.");
            Note secondNote = new Note("Second note", "I need to finish all my assignments.");
            Note thirdNote = new Note("Third note", "Quarantine is boring.");
            Notebook notebook = new Notebook();
            notebook.AddNote(firstNote);
            notebook.AddNote(secondNote);
            notebook.AddNote(thirdNote);
            notebook.AddNote(new Note("Fourth note", "LV6"));
            IAbstractIterator iterator = notebook.GetIterator();
            for(int i = 0; i<notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }
            notebook.RemoveNote(secondNote);
            IAbstractIterator secondIterator = notebook.GetIterator();
            for (int i = 0; i < notebook.Count; i++)
            {
                secondIterator.Current.Show();
                secondIterator.Next();
            }
            notebook.Clear();
            IAbstractIterator thirdIterator = notebook.GetIterator();
            for (int i = 0; i < notebook.Count; i++)
            {
                thirdIterator.Current.Show();
                thirdIterator.Next();
            }
        }
    }
}
