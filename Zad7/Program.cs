﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker DigitChecker = new StringDigitChecker();
            StringChecker LengthChecker = new StringLengthChecker();
            PasswordValidator validator = new PasswordValidator(DigitChecker);
            validator.SetNext(LengthChecker);
            validator.CheckPassword("rppoon123");
            validator.CheckPassword("rppoon");
        }
    }
}
