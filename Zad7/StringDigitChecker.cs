﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class StringDigitChecker: StringChecker
    {
        protected override bool PerformCheck(string stringtoCheck)
        {
            if (stringtoCheck.Any(char.IsDigit))
            {
                Console.WriteLine("String ima broj u sebi.");
                return true;
            }
            else
            {
                Console.WriteLine("String nema broj u sebi.");
                return false;
            }
        }
    }
}
