﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class PasswordValidator
    {
        private StringChecker first;
        public PasswordValidator(StringChecker checker)
        {
            this.first = checker;
        }
        public void SetNext(StringChecker next)
        {
            first.SetNext(next);
        }
        public bool CheckPassword(string PasswordToCheck)
        {
            if (first.Check(PasswordToCheck) == true)
            {
                Console.WriteLine("Lozinka je ispravna.");
                return true;
            }
            Console.WriteLine("Lozinka nije ispravna.");
            return false;
        }
    }
}
