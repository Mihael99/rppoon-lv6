﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class StringUpperCaseChecker: StringChecker
    {
        protected override bool PerformCheck(string stringtoCheck)
        {
            if (stringtoCheck.Any(char.IsUpper))
            {
                Console.WriteLine("String ima barem jedno veliko slovo");
                return true;
            }
            else
            {
                Console.WriteLine("String nema barem jedno veliko slovo");
                return false;
            }
        }
    }
}
